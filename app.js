'use strict'
require('dotenv').load()

const express = require('express')
const path = require('path')
const cors = require('cors')
const favicon = require('serve-favicon')
const logger = require('morgan')
const cookieParser = require('cookie-parser')
const bodyParser = require('body-parser')
const compress = require('compression')
const helmet = require('helmet')
const app = express()

const workers = require('./api/workers')
const relations = require('./api/relations')
const sites = require('./api/sites')

app.use(compress())

// some safety stuff not so safe for nom
const ninetyDaysInMilliseconds = 7776000000
app.use(helmet.hsts({
    maxAge: ninetyDaysInMilliseconds
}))
app.use(helmet.noCache())
app.use(helmet.frameguard())
app.use(helmet.noSniff())
app.use(helmet.xssFilter())
app.use(helmet.hidePoweredBy({
    setTo: 'PHP 7.0.0' // just cheekey stuff
}))
app.use(cors())

// uncomment after placing your favicon in /public
app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')))
app.use(logger('dev'))

// use parsers middleware for REST shit
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({
    extended: true
}))
app.use(cookieParser())

// return all sites in an array of objects
app.route('/api/site/').get(sites.getAll)

// return one site by id in the url
app.route('/api/site/:id').get(sites.getById)

// add site from an object creates a new id
// {"name": "test Site",
// "contact": { "email":"email@test.com",
// "phone": "09090909090" }}
app.route('/api/site/').post(sites.add)

// get site object with all its workers as part of the object
// think of thinky magic
app.route('/api/site/:id/:withWorkers').get(sites.getByIdAll)

// add worker to the site with id withWorkers body is an array of valid
// worker ids
app.route('/api/site/:id/:withWorkers').post(sites.relateWorkers)

// return all workers in the database
app.route('/api/worker/').get(workers.getAll)

// add one worker where data is a worker object or an array of worker objects
app.route('/api/worker/').post(workers.add)

app.route('/api/worker/').put(workers.update)
app.route('/api/worker/:id').delete(workers.delete)
app.route('/api/worker/:id').get(workers.getById)

// get all relations as an array of objects
app.route('/api/relation/').get(relations.getAll)
app.route('/api/relation/').post(relations.add)
app.route('/api/relation/:id').get(relations.getById)

// stacktrace
if (app.get('env') === 'development') {
    app.use(function (err, req, res, next) {
        res.status(err.status || 500)
        res.json({
            message: err.message,
            error: err
        })
    })
}

// production error handler no stacktraces leaked to user
app.use(function (err, req, res, next) {
    res.status(err.status || 500)
    res.json({
        message: err.message,
        error: err
    })
    next()
})

app.use(express.static(path.join(__dirname, 'public')))
module.exports = app
