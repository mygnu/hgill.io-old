var path = require('path')
var webpack = require('webpack')
var HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = {
    entry: {
        build: './client/main.js',
        vendor: ['d3', 'vue-resource', 'vue-mdl', 'material-design-lite']
    },
    output: {
        path: path.resolve(__dirname, './public'),
        publicPath: '/',
        filename: 'build.js'
    },
    resolveLoader: {
        root: path.join(__dirname, 'node_modules')
    },
    plugins: [
        new HtmlWebpackPlugin({ // generate index.html from a template
            // filename: 'index.html',
            template: 'client/index.temp.html',
            favicon: 'client/favicon.ico'
        }),
        new webpack.optimize.CommonsChunkPlugin('vendor', 'vendor.build.js')
    ],
    module: {
        loaders: [
            {
                test: /\.vue$/,
                loader: 'vue'
            }, {
                test: /\.js$/,
                loader: 'babel',
                exclude: /node_modules/
            }, {
                test: /\.json$/,
                loader: 'json'
            }, {
                test: /\.(png|jpg|gif|svg)$/,
                loader: 'url',
                query: {
                    limit: 10000,
                    name: '[name].[ext]?[hash]'
                }
            }
        ]
    },

    devServer: {
        historyApiFallback: true,
        noInfo: true
    },
    devtool: 'eval-source-map',

    vue: {
        loaders: {
            sass: 'vue-style!css!sass?indentedSyntax',
            scss: 'vue-style!css!sass'
        },
        autoprefixer: {
            browsers: ['last 20 versions']
        }
    }
}
if (process.env.NODE_ENV === 'production') {
    // module.exports.devtool = 'source-map'
        // http://vuejs.github.io/vue-loader/workflow/production.html
    module.exports.plugins = (module.exports.plugins || []).concat([
        new webpack.DefinePlugin({
            'process.env': {
                NODE_ENV: '"production"'
            }
        }),
        new webpack.optimize.UglifyJsPlugin({
            compress: {
                warnings: false
            }
        }),
        new webpack.optimize.OccurenceOrderPlugin()
    ])
}
