'use strict'
const path = require('path')
const shortid = require('shortid')
const handleError = require(path.join(__dirname, '/utils/handleError'))
const handleSucess = require(path.join(__dirname, '/utils/handleSucess'))
    // Relation = require(path.join(__dirname, '/models')).Relation,
const Site = require(path.join(__dirname, '/models')).Site

// returns an array of all site objects
exports.getAll = function (req, res) {
    Site.run().then(function (result) {
            handleSucess(res, result)
        })
        .error(handleError(res))
}

// gets one site object by its id
exports.getById = function (req, res) {
    const id = req.params.id
    Site.get(id).run()
        .then(function (result) {
            handleSucess(res, result)
        })
        .error(handleError(res))
}

// add site object to database
exports.add = function (req, res) {
    const data = req.body
    // console.log(data)
    // generate a short url friendly id if doesn't exist already
    data.id = data.id || shortid.generate()
    const site = new Site(data)
    site.save().then(function (result) {
            handleSucess(res, result)
        })
        .error(handleError(res))
}

// this function gets the site with all its workers
exports.getByIdAll = function (req, res) {
    const id = req.params.id
    Site.get(id).getJoin({
            'workers': true
        })
        .run().then(function (result) {
            handleSucess(res, result)
        })
        .error(handleError(res))
}
// workers to a site where workers are an array of ids
// these workers must exist in the database to ba added to a site
exports.relateWorkers = function (req, res) {
    const id = req.params.id
    const workers = req.body
    Site.get(id).run()
        .then(function (site) {
            console.log(JSON.stringify(site))
            var arrayLength = workers.length
            for (var i = 0; i < arrayLength; i++) {
                Site.get(id).addRelation('workers', {
                    id: workers[i]
                }).catch(console.error)
            }
            handleSucess(res)
        }).error(handleError(res))
}
