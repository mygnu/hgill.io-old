'use strict'
// I'm sure all these f
const path = require('path')
const shortid = require('shortid')
const handleError = require(path.join(__dirname, '/utils/handleError'))
const handleSucess = require(path.join(__dirname, '/utils/handleSucess'))
    // Relation = require(path.join(__dirname, '/models')).Relation,
const Worker = require(path.join(__dirname, '/models')).Worker

// Retrieve a list of nodes ordered by date with its author and comments
exports.getAll = function (req, res) {
    Worker.run().then(function (result) {
            handleSucess(res, result)
        })
        .error(handleError(res))
}

exports.getById = function (req, res) {
    const id = req.params.id
    Worker.get(id).run()
        .then(function (result) {
            handleSucess(res, result)
        })
        .error(handleError(res))
}

exports.add = function (req, res) {
    const data = req.body
    const saved = []
    if (data.isArray) {
        for (const value of data) {
            value.id = value.id || shortid.generate()
            const worker = new Worker(value)
            worker.save()
                .then(function (result) {
                    saved.push(result)
                })
                .error(handleError(res))
        }
        handleSucess(res, saved)
    } else if (typeof data === 'object') {
        // generate a short url friendly id
        data.id = data.id || shortid.generate()
        const worker = new Worker(data)

        worker.save()
            .then(function (result) {
                handleSucess(res, result)
            })
            .error(handleError(res))
    }
}

exports.update = function (req, res) {
    const data = req.body // Data posted by the user
        // if data doesn't have id
    if (data.id === undefined) {
        res.json({
            error: 'can\'t update without id'
        })
        return
    }

    Worker.get(data.id).run()
        .then(function (worker) {
            worker.merge(data).save()
                .then(function (result) {
                    res.json(result)
                        // post was updated with `data`
                }).error(handleError(res))
        }).error(handleError(res))
}

exports.delete = function (req, res) {
    const id = req.params.id
    Worker.get(id).then(function (worker) {
        worker.delete().then(function (result) {
            res.json(result)
        }).error(handleError(res))
    }).error(handleError(res))
}
