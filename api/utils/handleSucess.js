'use strict'

module.exports = function handleSucess(res, result) {
    return res.status(200).send({
        sucess: true,
        result: result || {}
    })
}
