'use strict'

module.exports = function handleError(res) {
    return function (error) {
        console.log(error.message)
        return res.status(500).send({
            success: false,
            error: error.message
        })
    }
}
