'use strict'
const path = require('path')
const thinky = require(path.join(__dirname, '/utils/thinky'))
    // var r = thinky.r
var type = thinky.type

// Create the models
// Note: if we don't provide the field date, the default function will be call
var Site = thinky.createModel('sites', {
    id: type.string(),
    name: type.string(),
    contact: type.object()
        .schema({
            email: type.string(),
            phone: type.string()
        })
})
Site.ensureIndex('name')

// model for a worker
var Worker = thinky.createModel('workers', {
    id: type.string(),
    name: type.string().required(),
    contact: type.object()
        .schema({
            email: type.string(),
            phone: type.string()
        }),
    member: type.boolean().required(),
    leader: type.boolean().default(false),
    siteId: type.string(),
    added: type.date()
})
Worker.ensureIndex('name')
    // relationship between sites and workers
Site.hasMany(Worker, 'workers', 'id', 'siteId')
Worker.belongsTo(Site, 'sites', 'siteId', 'id')

// Model for relations

var Relation = thinky.createModel('relations', {
    id: type.string().required(),
    source: type.string().required(),
    target: type.string().required(),
    type: type.string().default('employer')
})

module.exports = {
    Worker: Worker,
    Relation: Relation,
    Site: Site
}
