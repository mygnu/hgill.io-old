import d3 from 'd3'

// global vars

let svg = null
let svgg = null
let force = null
let svgEdges = null
let svgNodes = null
let svgLabels = null
let linkedByIndex = null
let nodes = []
let edges = []
let selectedNodes = []
const radius = 15
const color = d3.scale.category10()
const zoom = d3.behavior.zoom()
    .scaleExtent([-2, 10])
    .on('zoom', zoomed)

export function getState() {
    return {
        nodes: nodes,
        edges: edges
    }
}
let drag
export function init(elmId) {
    let width = window.innerWidth - (0.01 * window.innerWidth)
    let height = window.innerHeight - 50
    force = d3.layout.force()
        .size([width, height])
        .nodes(nodes)
        .links(edges)
        .start()
        .on('tick', () => {
            tickGraph()
        })
    drag = force.drag()
        .origin(function (d) {
            return d
        })
        .on('dragstart', dragStarted)
        .on('drag', dragged)
        .on('dragend', dragEnded)

    svg = d3.select(elmId)
        .append('svg')
        .attr('preserveAspectRatio', 'xMidYMid meet')
        // .attr('viewBox', '0 0 800 600')
        .attr('width', width)
        .attr('height', height)
        .call(zoom)

    svgg = svg.append('g')
    svgEdges = svgg.append('g').attr('class', 'edge')
    svgNodes = svgg.append('g').attr('class', 'node')
    svgLabels = svgg.append('g').attr('class', 'label')

    d3.select(window).on('resize', () => {
        width = window.innerWidth - (0.01 * window.innerWidth)
        height = window.innerHeight - 50
        svg.attr('width', width)
            .attr('height', height)
        force.size([width, height])
            .start()
    })
}

function zoomed() {
    const trans = d3.event.translate
    const scale = d3.event.scale
    svgg.attr('transform', 'translate(' + trans + ')' + ' scale(' + scale + ')')
}

function dragStarted(d) {
    d3.event.sourceEvent.stopPropagation()
    d3.select(this).classed('dragging', true)
    force.start()
}

function dragged(d) {
    // d3.select(this)
    //     .attr('cx', d3.event.x)
    //     .attr('cy', d3.event.y)
}

function dragEnded(d) {
    d3.select(this).classed('dragging', false)
}
export function reset() {
    nodes = []
    edges = []
    updateGraph()
}
export function updateGraph() {
    force
        .nodes(nodes)
        .links(edges)

    const edge = svgEdges.selectAll('.edge')
        .data(edges)
        .attr('class', 'edge')
        .classed({
            'employer': (d) => {
                return d.type === 'employer'
            },
            'friend': (d) => {
                return d.type === 'friend'
            },
            'casual': (d) => {
                return d.type === 'casual'
            }

        })

    edge
        .enter().append('line')
        .attr('class', 'edge')
        .classed({
            'employer': (d) => {
                return d.type === 'employer'
            },
            'friend': (d) => {
                return d.type === 'friend'
            },
            'casual': (d) => {
                return d.type === 'casual'
            }

        })
    edge
        .exit().remove()

    const node = svgNodes.selectAll('.node')
        .data(nodes)
        .attr('class', 'node')
        .attr('r', (d) => {
            if (d.site) {
                return radius + d.weight * 0.8
            }
            return radius
        })
        .attr('fill', fillNode)
        .call(drag)

    node.on('mouseover', function (d) {
        if (d3.event.ctrlKey) {
            node.classed('node-active', function (o) {
                const thisOp = isConnected(d, o)
                this.setAttribute('fill-opacity', thisOp)
                return thisOp
            })
            label.classed('label-active', function (o) {
                return isConnected(d, o)
            })
            edge.classed('edge-active', function (e) {
                return e.source === d || e.target === d
            })
            d3.select(this).classed('node-active', true)
                // d3.select(this).classed('label-active', true)
        }
    })

    node.on('mouseout', function (d) {
        if (d3.event.ctrlKey) {
            node.classed('node-active', false)
            edge.classed('edge-active', false)
            label.classed('label-active', false)
        }
    })
    node.on('mousedown', function (d) {
        if (d3.event.shiftKey) {
            if (selectedNodes.indexOf(d) === -1) {
                selectedNodes.push(d)
            }
        } else {
            selectedNodes = []
            selectedNodes.push(d)
        }
        d3.select(this).classed('node-active', true)
        console.log(selectedNodes)
    })
    node
        .enter().append('circle')
        .attr('class', 'node')
        .attr('r', (d) => {
            if (d.site) {
                return radius + d.weight * 0.8
            }
            return radius
        })
        .attr('fill', fillNode)
        // .classed('site', (d) => {
        //     return d.site
        // })
        // .call(force.drag)

    node
        .exit().remove()

    const label = svgLabels.selectAll('.label')
        .data(nodes)
        .attr('dx', (d) => {
            if (d.site) {
                return radius + d.weight * 0.8 + 5
            }
            return radius + 5
        })
        .text((d) => {
            return d.name
        })

    label
        .enter().append('svg:text')
        .text((d) => {
            return d.name
        })
        .attr('dx', (d) => {
            if (d.site) {
                return radius + d.weight * 0.8 + 5
            }
            return radius + 5
        })
        .attr('class', 'label')
        .on('click', (d) => {
            if (d3.event.defaultPrevented) return
            console.log('click node ', d)
                // self.$dispatch('graph-node-clicked', d)
        })

    label
        .exit().remove()

    force
        .gravity(0.1)
        .charge(-5000)
        .friction(0.3)
        .linkDistance((d) => {
            return d.value || 200
        })
        .start()
    linkedByIndex = {}
    edges.forEach((d) => {
        linkedByIndex[d.source.index + ',' + d.target.index] = 1
    })
}
d3.select('body').on('keydown', function () {
    if (d3.event.keyCode === 27) {
        svgNodes.selectAll('.node')
            .classed('node-active', false)
        svgNodes.selectAll('.edge')
            .classed('edge-active', false)
        svgNodes.selectAll('.label')
            .classed('label-active', false)

        selectedNodes = []
    }
})

function fillNode(d) {
    if (d.site) {
        return color('site')
    } else if (d.leader) {
        return color('leader')
    } else if (d.member) {
        return color('member')
    }
    return color('non member')
}

function isConnected(a, b) {
    return linkedByIndex[a.index + ',' + b.index] || linkedByIndex[b.index + ',' + a.index]
}

export function addNode(obj) {
    nodes.push(obj)
    updateGraph()
}

function nodeIndex(objId) {
    const pos = nodes.map((x) => {
        return x.id
    }).indexOf(objId)

    if (pos !== -1) {
        return pos
    }
    return null
}

export function updateNode(obj) {
    const elementPos = nodeIndex(obj.id)

    if (elementPos !== null) {
        const node = nodes[elementPos]
        for (const propName in obj) {
            if (obj.hasOwnProperty(propName)) {
                node[propName] = obj[propName]
            }
        }
    }
    // console.log(nodes[elementPos])
    updateGraph()
}

export function removeNode(nodeId) {
    const index = nodeIndex(nodeId)
    let i = 0
    const n = getNode(nodeId)
    if (index) {
        while (i < edges.length) {
            if ((edges[i]['source'] === n) || (edges[i]['target'] === n)) {
                edges.splice(i, 1)
            } else i++
        }
        nodes.splice(index, 1)
        updateGraph()
    }
}

export function addEdge(obj) {
    obj.source = getNode(obj.source)
    obj.target = getNode(obj.target)
    obj.value = obj.value || 200
    if (!obj.source || !obj.target) {
        return
    }
    edges.push(obj)
    updateGraph()
}

export function removeEdgeBylinks(sourceId, targetId) {
    for (let i = 0; i < edges.length; i++) {
        if (edges[i]['source']['id'] === sourceId && edges[i]['target']['id'] === targetId) {
            edges.splice(i, 1)
            break
        }
    }
    updateGraph()
        // console.log(this._edges)
}

export function removeEdgeById(edgeId) {
    for (let i = 0; i < edges.length; i++) {
        if (edges[i]['id'] === edgeId) {
            edges.splice(i, 1)
            break
        }
    }
    updateGraph()
}

function getNode(id) {
    for (const i in nodes) {
        if (nodes[i]['id'] === id) return nodes[i]
    }
    return null
}

function tickGraph() {
    // this._force.start()
    svgEdges.selectAll('.edge')
        .attr('x1', (d) => {
            return d.source.x
        })
        .attr('y1', (d) => {
            return d.source.y
        })
        .attr('x2', (d) => {
            return d.target.x
        })
        .attr('y2', (d) => {
            return d.target.y
        })
        // .attr("stroke-width", function(d) {
        //     return d.value
        // })

    svgNodes.selectAll('.node')
        .attr('transform', (d) => {
            return 'translate(' + d.x + ',' + d.y + ')'
        })
    svgLabels.selectAll('.label')
        .attr('transform', (d) => {
            return 'translate(' + d.x + ',' + d.y + ')'
        })
}
